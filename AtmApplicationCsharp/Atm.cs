﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmApplicationCsharp
{

    /// <summary>
    /// The Atm class representing an ATM machine. The class displays and performs the the account management functions
    /// on a given bank account: checking balance, withdrawing and depositing money
    /// </summary>
    class Atm
    {
        //the bank this ATM object is working with
        public Bank _bank;

        //create the MAIN MENU options
        public int selectAccountOption;
        public int createAccountOption;
        public int exitAtmApplicationOption;

        //create ACCOUNT MENU option
        public int checkBalanceOption;
        public int withdrawOption;
        public int depositOption;
        public int exitAccountOption;


        public string clientName;

        public Atm(Bank bank)
        {
            //the bank this ATM object is working with
            _bank = bank;

            //create the MAIN MENU options
            selectAccountOption = 1;
            createAccountOption = 2;
            exitAtmApplicationOption = 3;

            //create ACCOUNT MENU option
            checkBalanceOption = 1;
            withdrawOption = 2;
            depositOption = 3;
            exitAccountOption = 4;

        }

        //Starts the ATM program by displaying the required user options. 
        //User navigates the menus managing their accounts
        public void Start()
        {
            //TODO: display the main menu and perform the main actions depending on the user's choice
        }

        //Displays the main ATM menu and ensure the user picks an option. Handles invalid input but doesn't check
        //that the menu option is one of the displayed ones.
        public void showMainMenu()
        {
            
        }

        //Displays the ACCOUNT menu that allows the user to perform account operations. Handles invalid input but doesn't check
        //that the menu option is one of the displayed ones.
        public void showAccountMenu()
        {

        }

        //Create and open an account. The user is prompted for all account information including the type of account to open.
        //Create the account object and add it to the bank
        public void onCreateAccount()
        {
            //TODO: Get properties for account from user
        }

        //Select an account by prompting the user for an account number and remembering which account was selected.
        //Prompt the user for performing account information such deposit and withdrawals
        public void selectAccount()
        {
            //TODO: obtain the account required by the user from the bank
        }

        /// <summary>
        /// Manage the account by allowing the user to execute operation on the given account
        /// </summary>
        /// account - the account to be managed
        public void manageAccount(Account account)
        {
            
        }

        /// <summary>
        /// Prints the balance in the given account
        /// </summary>
        /// <param name="account">the account for which the balance is printed  </param>
        public void onCheckBalance(Account account)
        {
            //TODO: Give a print statement
        }

        /// <summary>
        ///  Prompts the user for an amount and performs the deposit. Handles any errors related to incorrect amounts
        /// </summary>
        /// <param name="account">the account in which the amount is to be deposited</param>
        public void onDeposit(Account account)
        {
            //TODO: Let the user deposit an amount, also check for unwanted amounts
        }

        /// <summary>
        /// Prompts the user for an amount and performs the withdrawal. Handles any errors related to incorrect amounts
        /// </summary>
        /// <param name="account">the account in which the amount is to be withdrawn</param>
        public void onWithdraw(Account account)
        {
            //TODO: Let the user withdraw a desired amount 
        }

        /// <summary>
        /// 
        /// </summary>
        public void onDisplayTransactions()
        {

        }

        /// <summary>
        /// Prompts the user to enter the name of the client and allows the user to cancel by pressing ENTER
        /// </summary>
        /// <returns>clientName</returns>
        public string promptForClientName()
        {
            return clientName;
        }

        /// <summary>
        /// Prompts the user to enter an account balance and performs basic error checking
        /// </summary>
        public void promptForDepositAmount()
        {
            
        }

        /// <summary>
        /// Prompts the user to enter the annual interest rate for an account
        /// </summary>
        public void promptForAnnualIntrRate()
        {
         
        }

        /// <summary>
        /// Prompts the user to enter an account type
        /// </summary>
        public int promptForAccountType(Account account)
        {
            return accountTypeChequing;//TODO fix this
        }

    }
}
