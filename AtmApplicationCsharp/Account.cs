﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmApplicationCsharp
{

    /// <summary>
    /// Defines a bank account its associated attributes and operations.
    /// </summary>
    class Account
    {

        /// <summary>
        /// constant representing a checquing account type
        /// </summary>
        public int accountTypeChequing = 1;

        /// <summary>
        /// constant representing a savings account type
        /// </summary>
        public int accountTypeSavings = 2;

        public int _acctNo;

        public string _acctHolderName;

        public float _balance;

        public float _annualIntrRate;

        /// <summary>
        /// Initialize the account object with its attributes.
        /// </summary>
        public Account(int acctNo, string acctHolderName)
        {

            _acctNo = acctNo;
            _acctHolderName = acctHolderName;
            _balance = 0.0F;
            _annualIntrRate = 0.0F;
        }

        /// <summary>
        /// Return the account number.
        /// </summary>
        /// <returns></returns>
        public int getAccountNumber()
        {
            return _acctNo;
        }

        /// <summary>
        /// Return the account holder's name
        /// </summary>
        /// <returns></returns>
        public string getAcctHolderName()
        {
            return _acctHolderName;
        }

        /// <summary>
        /// Return the balance in the account
        /// </summary>
        /// <returns></returns>
        public float getBalance()
        {
            return _balance;
        }

        /// <summary>
        /// Return the annuaul interest rate on the account
        /// </summary>
        /// <returns></returns>
        public float getAnnualIntrRate()
        {
            return _annualIntrRate;
        }

        /// <summary>
        /// Change the annual interest rate on the account
        /// </summary>
        /// <param name="newAnnualIntrRatePercentage"></param>
        public void setAnnualIntrRate(float newAnnualIntrRatePercentage)
        {

        }

        /// <summary>
        /// Calculate and return the monthly interest rate on the account
        /// </summary>
        /// <returns></returns>
        public float getMonthlyIntrRate()
        {
            return _annualIntrRate;
        }

        /// <summary>
        /// Deposit the given amount in the account and return the new balance
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public float deposit(float amount)
        {
            //TODO: check that the amount is positive, change the balance, provide the new balance to the caller to avoid a getBalance() call
            return _balance;
        }

        /// <summary>
        /// Withdraw the given amount from the account and return teh new balance
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public float withdraw(float amount)
        {
            //TODO: Take out money from the account
            return _balance;
        }

        /// <summary>
        /// Load the account information from the given file. The file is assumed opened
        /// </summary>
        public void load()
        {
            //TODO: read the account properties in the same order they were saved
        }

        /// <summary>
        /// Save the account information in the given file. The file is assumed opened.
        /// </summary>
        public void save()
        {
            //TODO: write the account properties, one per line
        }
    }
}
