﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmApplicationCsharp
{
    /// <summary>
    /// Represents a bank composed of a list of accounts.
    /// </summary>
    class Bank
    {
        public float DEFAULT_ACCT_NO_START;

        private object newAccount;

        //the list of accounts managed by the bank
        public List<Bank> _accountList;

        //The number of the current account
        public int acctNo;

        public Bank()
        {
            //Initialize the field variables of the bank object, the account of the bank
            DEFAULT_ACCT_NO_START = 100F;

            _accountList = new List<Bank>();
        }

        /// <summary>
        /// Load the account data for all the accounts. The account data files are stored in a directory
        /// named BankingData located in the current directory, the directory used to run the application from
        /// </summary>
        public void loadAccountData()
        {

            //TODO: get the list of files in the directories, go through the list of files.

            //TODO: read the account type and create the correct account, load the data into the account object, add the account to the list of accounts

        }

        /// <summary>
        /// Saves the data for all accounts in the data directory of the application. Each account is
        /// saved in a separate file which contains all the account information.The account data files are stored in a
        /// directory named BankingData located in the current directory, the directory used to run the application from
        /// </summary>
        public void saveAccountData()
        {

            //TODO: make the directory if it does not exist

            //TODO: go through each account in the list of accounts and ask it to save itself into a corresponding file

            //TODO:by using context manager for the file that will automatically close the file at the end of the with eblock

        }

        /// <summary>
        /// Create 10 accounts with predefined IDs and balances. The default accounts are created only
        /// if no account data files exist
        /// </summary>
        public void createDefaultAccount()
        {
            //TODO: create the account with required properties, and add the he account to the list.
        }

        /// <summary>
        /// Returns the account with the given account number or null if no account with that ID can be found
        /// </summary>
        /// <param name="acctNo">the account number of the account to return</param>
        public void findAccount(int acctNo)
        {
            //TODO: go through all the accounts until one is found with the given account number
        }

        /// <summary>
        /// Determine the account number prompting the user until they enter the correct information
        /// </summary>
        public int determineAccountNumber()
        {
            //TODO: ask the user for input, check the input to ensure correctness and deal with incorrect input, check that the account number is not in use
            return acctNo;
        }

        //Create and store an account objec with the required attributes
        public void openAccount(string clientName, int acctType)
        {
            //TODO: prompt the user for an account number, create and store an account object with the required attributes

            //return the account to the caller so other properties can be set
            return newAccount;
        }
    }
}
