﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmApplicationCsharp
{
    class ChequingAccount
    {
        public int overdraftLimit = 500;

        public float maxInterestRate = 1.0F;
        private float _balance;

        /// <summary>
        /// Provide a constructor and ensure the base contstructor gets called
        /// </summary>
        /// <param name="acctNo">the account number</param>
        /// <param name="acctHolderName">the name of the account holder</param>
        public ChequingAccount(int acctNo=-1, string acctHolderName=" ")
        {
            
        }

        /// <summary>
        /// Change the annual interest rate on the account. Verify the annual interest rate is valid for a checquing account
        /// </summary>
        /// <param name="newAnnualIntrRatePercentage">the annual interest as a percentage (e.g. 3%)</param>
        public void setAnnualIntrRate(float newAnnualIntrRatePercentage)
        {
            //TODO: check to ensure the annual interest rate is valid for a checquing account, use the base class to set the annual interest rate
        }

        /// <summary>
        /// Withdraw the given amount from the account and return teh new balance
        /// </summary>
        /// <param name="amount">the amount to be withdrawn, cannot be negative or greater than balance and overdraft combined  </param>
        public float withdraw(float amount)
        {
            
            return _balance;    
        }

    }
}
