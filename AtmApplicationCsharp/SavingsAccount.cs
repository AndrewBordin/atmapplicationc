﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmApplicationCsharp
{
    class SavingsAccount
    {

        /// <summary>
        /// The matching deposit ratio. For every dollar deposit this account will automatically be credited with 0.5 dollars.
        /// </summary>
        public float _matchingDepositRatio;

        /// <summary>
        /// The minimmum interest rate for savings accounts.
        /// </summary>
        public float _minInterestRate;
        private float _balance;


        /// <summary>
        /// Provide a constructor and ensure the base contstructor gets called
        /// </summary>
        /// <param name="accountNo"></param>
        /// <param name="acctHolderName"></param>
        public SavingsAccount(int accountNo=1 , string acctHolderName="")
        {
            _matchingDepositRatio = 0.5F;

            _minInterestRate = 3.0F;

        }

        /// <summary>
        /// Change the annual interest rate on the account. Verify the annual interest rate is valid for a savings account
        /// </summary>
        /// <param name="newAnnualIntrRatePercentage"></param>
        public void setAnnualIntrRate(float newAnnualIntrRatePercentage)
        {
            //TODO: check to ensure the annual interest rate is valid for a checquing account, use the base class to set the annual interest rate
        }

        /// <summary>
        /// Deposit the given amount in the account and return the new balance.
        /// </summary>
        /// <param name="amount"></param>
        public float deposit(float amount)
        {
            //returns the balance with the amount added to it.
            return _balance;
        }



    }
}
